<?php

/**
 * Implements hook_menu().
 */
function domain_google_tag_menu() {
  if (_domain_google_tag_module_can_be_used()) {
    $items['admin/structure/domain/view/%domain/google_tag'] = array(
      'title' => 'Google tag manager',
      'description' => 'Google tag manager code on domain level',
      'access arguments' => array('access domain google tag'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('domain_google_tag_settings_form', 4),
      'type' => MENU_LOCAL_TASK,
      'weight' => 10,
    );

    return $items;
  }
}

/**
 * Calls form elements from google_tag module and add some values.
 *
 * @param string $form
 *   Build the form.
 * @param string $form_state
 *   Store the form values.
 * @param string $domain
 *   Creates the domain.
 *
 * @return mixed
 *   Return data if works fine.
 */
function domain_google_tag_settings_form($form, &$form_state, $domain) {
  global $_domain;
  $active_domain = $_domain;
  $_domain = $domain;

  $form['#domain'] = $domain;
  $form['domain_id'] = array(
    '#type' => $domain['domain_id'],
  );

  form_load_include($form_state, 'inc', 'google_tag', 'includes/admin');

  $form = google_tag_settings_form($form, $form_state);
  $form['#realm_keys'] = array($domain['domain_id'] => $domain['domain_id']);
  $form['#validate'] = array('google_tag_settings_form_validate');
  $form['#submit'] = array('domain_google_tag_settings_form_submit');
  unset($form['#after_build']);

  _domain_google_tag_set_default_values($domain, $form);

  $_domain = $active_domain;

  return $form;
}

/**
 * Loops through the form to set default values.
 *
 * @param string $domain
 *   Set default value.
 * @param string $elements
 *   Getting elements.
 */
function _domain_google_tag_set_default_values($domain, &$elements) {
  $element_children = element_children($elements);
  foreach ($element_children as $element_child) {
    $element = &$elements[$element_child];
    _domain_google_tag_set_default_values($domain, $element);
    if (isset($element['#default_value'])) {
      $element['#default_value'] = domain_conf_variable_get($domain['domain_id'], $element_child);
    }
  }
}

/**
 * Form submission handler for domain_google_tag_settings_form().
 */
function domain_google_tag_settings_form_submit(&$form, &$form_state) {
  global $_domain;
  $active_domain = $_domain;
  $_domain = $form['#domain'];

  foreach ($form_state['values'] as $field => $value) {
    if ($field != 'domain_id') {
      domain_conf_variable_set($_domain['domain_id'], $field, $value);
    }
  }

  google_tag_settings_form_submit($form, $form_state);
  $_domain = $active_domain;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function domain_google_tag_form_domain_google_tag_settings_form_alter(&$form, &$form_state, $form_id) {
  unset($form['domain_settings']);
}

/**
 * Implements hook_module_implements_alter().
 */
function domain_google_tag_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'form_alter' && isset($implementations['domain_google_tag'])) {
    $group = $implementations['domain_google_tag'];
    unset($implementations['domain_google_tag']);
    $implementations['domain_google_tag'] = $group;
  }
}

/**
 * Implements hook_permission().
 */
function domain_google_tag_permission() {
  $permissions = array(
    'access domain google tag' => array(
      'title' => t('Access domain google tag'),
      'description' => t('Allows users to specify the google tag manager code per domain'),
      'restrict access' => TRUE,
    ),
  );

  return $permissions;
}

/**
 * Implements hook_google_tag_variable_alter().
 */
function domain_google_tag_google_tag_variable_alter(&$variable_value, $variable, $default = '') {
  if (_domain_google_tag_module_can_be_used()) {
    $domain = domain_get_domain();
    $domain_value = domain_conf_variable_get($domain['domain_id'], $variable);
    $variable_value = $domain_value ? $domain_value : '';
  }
}

/**
 * Implements hook_google_tag_realm_alter().
 */
function domain_google_tag_google_tag_realm_alter(&$realm) {
  if (_domain_google_tag_module_can_be_used()) {
    $domain = domain_get_domain();

    // We use the domain id for both values because machine name is changeable.
    $realm['name'] = $domain['domain_id'];
    $realm['key'] = $domain['domain_id'];
  }
}

/**
 * Helper callback to check if this module can and should be used with google tag.
 *
 * Note that the variable_realm and variable_store module are actually the
 * recommended paths to use google tag with multi domain sites.
 *
 * @return bool
 *   Return the boolean based on conditions.
 */
function _domain_google_tag_module_can_be_used() {
  if (!module_exists('variable_realm') || !module_exists('variable_store')) {
    return TRUE;
  }
  return FALSE;
}
