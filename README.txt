This module is an extension to the Google Tag module
for multi domain support.
The following patch https://www.drupal.org/node/2880710#comment-12142739
is required before this module will fully work.

After enabling the module you be able to configure google tag settings at
/admin/structure/domain/view/{domain_id}/google_tag
